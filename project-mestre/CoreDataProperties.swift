import Foundation
import CoreData


extension Photo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Photo> {
        return NSFetchRequest<Photo>(entityName: "Photo")
    }

    @NSManaged public var textName: String?
    @NSManaged public var textDescription: String?
    @NSManaged public var textAdress: String?
    @NSManaged public var image: NSData?

}
