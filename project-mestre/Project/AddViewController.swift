import UIKit
import CoreData

class AddViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var descriptionField: UITextField!
    @IBOutlet weak var adressField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    @IBOutlet weak var btn3: UIButton!
    
    var pc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var item: Photo? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btn1.layer.cornerRadius = 5
        btn3.layer.cornerRadius = 5
        
        if item == nil {
            self.navigationItem.title = "Registro"
        } else {
            //self.navigationItem.title = item?.textName
            self.navigationItem.title = "Registro"
            btn3.setTitle("Atualizar", for: .normal)
            
            nameField.text = item?.textName
            descriptionField.text = item?.textDescription
            adressField.text = item?.textAdress
            imageView.image = UIImage(data: (item?.image)! as Data)
        }
    }
    
    @IBAction func dismissKB(_ sender: Any) {
        self.resignFirstResponder()
    }
    
    @IBAction func library(_ sender: Any) {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
        pickerController.allowsEditing = true
        
        self.present(pickerController, animated: true, completion: nil)
    }
    
    @IBAction func camera(_ sender: Any) {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.sourceType = UIImagePickerController.SourceType.camera
        pickerController.allowsEditing = true
        
        self.present(pickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.image = image
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func save(_ sender: Any) {
        
        guard let title = nameField.text, !title.isEmpty else {
            self.simpleAlert(message: "Campo título obrigatório'")
            return
        }
        
        if item == nil {
            let entityDescription = NSEntityDescription.entity(forEntityName: "Photo", in: pc)
            let item = Photo(entity: entityDescription!, insertInto: pc)
            
            item.textName = nameField.text
            item.textDescription = descriptionField.text
            item.textAdress = adressField.text
            item.image = imageView.image?.pngData() as NSData?
        } else {
            item?.textName = nameField.text
            item?.textDescription = descriptionField.text
            item?.textAdress = adressField.text
            item?.image = imageView.image?.pngData() as NSData?
        }
        
        do {
            try pc.save()
        } catch {
            debugPrint(error.localizedDescription)
            return
        }
        
        navigationController?.popViewController(animated: true)
    }
    
    func simpleAlert(message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
}
